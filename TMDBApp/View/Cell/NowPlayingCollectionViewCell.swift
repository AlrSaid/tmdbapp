//
//  NowPlayingCollectionViewCell.swift
//  TMDBApp
//
//  Created by Said Alır on 6.06.2021.
//

import UIKit
import Kingfisher

class NowPlayingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(model: NowPlayingModel) {
        self.titleLabel.text = model.title
        if let path = model.img, let url = URL(string: "\(APIConstants.posterBaseUrl)\(path)") {
            self.posterImageView.kf.setImage(with: url, placeholder: UIImage(named: "cinema"))
        }
    }

}
