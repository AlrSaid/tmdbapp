//
//  TMDBAppTests.swift
//  TMDBAppTests
//
//  Created by Said Alır on 4.06.2021.
//

import XCTest
@testable import TMDBApp

class TMDBAppTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testGetMovieManager() {
        let id = 550//ID of fightclub
        var testModel: MovieModel?
        let exp = expectation(description: "success")
        MovieManager.shared.getMovieDetail(movieId: id) { model in
            testModel = model
            exp.fulfill()
        } failure: { _ in
            
        }
        waitForExpectations(timeout: 20) { _ in
            XCTAssertNotNil(testModel)
            XCTAssertEqual(testModel?.title, "Fight Club")
        }

    }
    
    func testGetSimilarMovies() {
        let id = 550
        var testModel: [SimilarMovie]?
        let exp = expectation(description: "success")
        MovieManager.shared.getSimilarMovies(movieId: id) { model in
            testModel = model
            exp.fulfill()
        } failure: { _ in

        }
        
        waitForExpectations(timeout: 20) { _ in
            XCTAssertNotNil(testModel)
            XCTAssertFalse(testModel!.isEmpty)
            XCTAssertFalse(testModel!.count < 3)
        }
    }
    
    func testGetUpcoming() {
        var testModel: [ListModel]?
        let exp = expectation(description: "success")
        ListManager.shared.getUpcomingData { model in
            testModel = model
            exp.fulfill()
        } failure: { _ in
            
        }
        
        waitForExpectations(timeout: 20) { _ in
            XCTAssertNotNil(testModel)
            XCTAssertFalse(testModel!.isEmpty)
            XCTAssertFalse(testModel!.count < 3)
        }
    }
    
    func testGetNowPlaying() {
        var testModel: [NowPlayingModel]?
        let exp = expectation(description: "success")
        ListManager.shared.getNowPlaying { model in
            testModel = model
            exp.fulfill()
        } failure: { _ in
            
        }
        
        waitForExpectations(timeout: 20) { _ in
            XCTAssertNotNil(testModel)
            XCTAssertFalse(testModel!.isEmpty)
            XCTAssertFalse(testModel!.count < 3)
        }
    }
    
    func testGetSearchResult() {
        let searchText = "Fight"
        var testModel: [SearchResult]?
        let exp = expectation(description: "success")
        ListManager.shared.getSearchResult(searchText: searchText) { model in
            testModel = model
            exp.fulfill()
        } failure: { _ in
            
        }

        waitForExpectations(timeout: 20) { _ in
            XCTAssertNotNil(testModel)
            XCTAssertFalse(testModel!.isEmpty)
            XCTAssertFalse(testModel!.count < 3)
        }
    }
}
