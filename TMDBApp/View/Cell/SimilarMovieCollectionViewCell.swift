//
//  SimilarMovieCollectionViewCell.swift
//  TMDBApp
//
//  Created by Said Alır on 5.06.2021.
//

import UIKit
import Kingfisher

class SimilarMovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var posterView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        self.mainView.layer.cornerRadius = 5.0
        self.mainView.layer.masksToBounds = true
        self.mainView.clipsToBounds = true
        self.posterView.layer.cornerRadius = 5.0
        self.posterView.layer.masksToBounds = true
        self.posterView.clipsToBounds = true
        self.titleLabel.adjustsFontSizeToFitWidth = true
    }
    
    func configureCell(model: SimilarMovie) {
        self.titleLabel.text = model.title
        self.dateLabel.text = "(\(model.date.prefix(4)))"
        if let img = model.image, let url = URL(string: "\(APIConstants.posterBaseUrl)/\(img)") {
            self.posterView.kf.setImage(with: url, placeholder: UIImage(named: "cinema"))
        }
    }

}
