//
//  ListMovieTableViewCell.swift
//  TMDBApp
//
//  Created by Said Alır on 6.06.2021.
//

import UIKit
import Kingfisher

class ListMovieTableViewCell: UITableViewCell {

    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        self.titleLabel.adjustsFontSizeToFitWidth = true
        self.detailLabel.adjustsFontSizeToFitWidth = true
        self.posterImage.layer.cornerRadius = 5.0
        self.posterImage.layer.masksToBounds = true
        self.posterImage.clipsToBounds = true
    }
    
    func configureCell(model: ListModel) {
        self.titleLabel.text = model.title
        self.dateLabel.text = model.date
        self.detailLabel.text = model.detail
        if let path = model.img, let url = URL(string: "\(APIConstants.posterBaseUrl)\(path)") {
            self.posterImage.kf.setImage(with: url, placeholder: UIImage(named: "cinema"))
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
