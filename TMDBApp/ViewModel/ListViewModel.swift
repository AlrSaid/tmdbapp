//
//  ListViewModel.swift
//  TMDBApp
//
//  Created by Said Alır on 6.06.2021.
//

import Foundation
import UIKit

class ListViewModel {
    
    var nowPlayingItems: [NowPlayingModel] = [] {
        didSet {
            bindNowPlaying()
        }
    }
    var upcomingItems: [ListModel] = [] {
        didSet {
            bindUpcoming()
        }
    }
    var bindUpcoming: (() -> ()) = {}
    var bindNowPlaying: (() -> ()) = {}
    
    init() {

    }
    
    func fetchData() {
        ListManager.shared.getUpcomingData { model in
            self.upcomingItems = model
        } failure: { error in
            print(error)
        }
        
        ListManager.shared.getNowPlaying { model in
            self.nowPlayingItems = model
        } failure: { error in
            print(error)
        }
    }
    
    func openSelectedMovie(movieId: Int,
                           vc: MovieDetailViewController,
                           navigationController: UINavigationController?,
                           success: @escaping () -> (),
                           failure: @escaping (NetworkError) ->()) {
        MovieManager.shared.getMovieDetail(movieId: movieId) { model in
            let vm = MovieDetailViewModel(model: model)
            vc.viewModel = vm
            navigationController?.pushViewController(vc, animated: true)
            success()
        } failure: { error in
            failure(error)
        }

    }
}
