//
//  BaseViewController.swift
//  TMDBApp
//
//  Created by Said Alır on 6.06.2021.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showErrorPopup(_ message: String) {
        let ok = NSLocalizedString("OK!", comment: "")
        let error = NSLocalizedString("Error", comment: "")
        let alertController = UIAlertController(title: error, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: ok, style: .default) { _ in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }

}
