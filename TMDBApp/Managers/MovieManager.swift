//
//  MovieManager.swift
//  TMDBApp
//
//  Created by Said Alır on 5.06.2021.
//

import Foundation
import Alamofire

class MovieManager {
    
    static let shared = MovieManager()
    
    func getMovieDetail(movieId: Int, success: @escaping (MovieModel) -> (), failure: @escaping (NetworkError) -> ()) {

        let str = "\(APIConstants.movieBaseUrl)" +
            "\(movieId)" +
            "\(QueryParameters.apiKey.rawValue)" +
            "\(APIConstants.apiKey)"
        
        guard let url = URL(string: str) else {
            failure((0, "url init error"))
            return
        }
        AF.request(url).validate().responseJSON { response in
            if let error = response.error {
                failure((error.responseCode, error.localizedDescription))
            }
            if let data = response.data {
                do {
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(MovieModel.self, from: data)
                    success(model)
                } catch {
                    failure((0, "data parse error"))
                }
            }
        }
    }
    
    func getSimilarMovies(movieId: Int, success: @escaping ([SimilarMovie]) -> (), failure: @escaping (NetworkError) -> ()) {
        let str = "\(APIConstants.movieBaseUrl)" +
            "\(movieId)" +
            "\(QueryParameters.similar.rawValue)" +
            "\(QueryParameters.apiKey.rawValue)" +
            "\(APIConstants.apiKey)"
        guard let url = URL(string: str) else {
            failure((0, "url init error"))
            return
        }
        AF.request(url).validate().responseJSON { response in
            if let error = response.error {
                failure((error.responseCode, error.localizedDescription))
            }
            if let data = response.data {
                do {
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(SimilarMovieResponse.self, from: data)
                    if let similarMovies = model.results {
                        success(similarMovies)
                    } else {
                        failure((0, "no similar movie found!"))
                    }
                  
                } catch {
                    failure((0, "data parse error"))
                }
            }
        }
    }
    
}
