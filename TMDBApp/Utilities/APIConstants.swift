//
//  APIManager.swift
//  TMDBApp
//
//  Created by Said Alır on 5.06.2021.
//

import Foundation

class APIConstants {
    
    static let baseUrl = "https://api.themoviedb.org/3/"
    static let movieBaseUrl = "https://api.themoviedb.org/3/movie/"
    static let apiKey = "31a0feec068e83924e64de0114a0b96a" //themoviedb.com you can create your own apikey
    static let posterBaseUrl = "https://image.tmdb.org/t/p/w500"
    static let imdbBase = "https://www.imdb.com/title/"
}
