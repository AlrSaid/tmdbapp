## The Movie Database App ##

It is created to show iOS Development Skills such as,

* Design Pattern
	* MVVM
	
* UIKit
	* UIStoryboard
	* UINavigationViewController
	* UITableViewController
	* UICollectionViewController
	* UISearchController
	* UIPageControl
	* UILabel, UIButton, etc.

* Third-Party Dependencies
	* Alamofire
	* Kingfisher

* Testing
	* XCTest
	

### What was considered? ###

* Set ratio based constraints to prevent unwanted UI in different devices
* Followed naming conventions
* Try to build on neat and clear structure

### Effort ###

* Finished in 11 hrs.