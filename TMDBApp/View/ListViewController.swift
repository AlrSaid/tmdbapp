//
//  ListViewController.swift
//  TMDBApp
//
//  Created by Said Alır on 5.06.2021.
//

import UIKit

class ListViewController: BaseViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var nowPlayingCV: UICollectionView!
    var searchController: UISearchController!
    var viewModel: ListViewModel!
    var tableData: [ListModel] = [] {
        didSet {
            self.listTableView.reloadData()
        }
    }
    var collectionData: [NowPlayingModel] = [] {
        didSet {
            self.nowPlayingCV.reloadData()
            self.pageControl.numberOfPages = collectionData.count
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initSearchBar()
        self.setupTable()
        self.setupCollectionView()
        self.initViewModel()
    }
   
    func initSearchBar() {
        self.navigationItem.title = "TMDB App"
        navigationItem.hidesSearchBarWhenScrolling = false
        let result = ResultTableViewController()
        result.delegate = self
        self.searchController = UISearchController(searchResultsController: result)
        self.navigationItem.searchController = searchController
        self.searchController.searchResultsUpdater = self
    }
    

    func initViewModel() {
        self.viewModel = ListViewModel()
        viewModel.bindUpcoming = {
            self.tableData = self.viewModel.upcomingItems
        }
        viewModel.bindNowPlaying = {
            self.collectionData = self.viewModel.nowPlayingItems
        }
        viewModel.fetchData()
    }

    func setupTable() {
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
        self.listTableView.register(UINib(nibName: "ListMovieTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "list")
    }
    
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: 250)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        self.nowPlayingCV.collectionViewLayout = layout
        self.nowPlayingCV.delegate = self
        self.nowPlayingCV.dataSource = self
        self.nowPlayingCV.register(UINib(nibName: "NowPlayingCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "nowPlaying")

    }
   

}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "list") as? ListMovieTableViewCell else {
            return UITableViewCell()
        }
        cell.configureCell(model: tableData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let id = self.tableData[indexPath.row].id
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as? MovieDetailViewController {
            self.viewModel.openSelectedMovie(movieId: id, vc: vc, navigationController: self.navigationController) {
                //successfully opened
            } failure: { error in
                let errMsg = "\(error.1) (Code: \(error.0 ?? 0))"
                self.showErrorPopup(errMsg)
            }
        }

    }
}

extension ListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nowPlaying", for: indexPath) as? NowPlayingCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.configureCell(model: collectionData[indexPath.row])
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let id = collectionData[indexPath.row].id
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as? MovieDetailViewController {
            self.viewModel.openSelectedMovie(movieId: id, vc: vc, navigationController: navigationController) {
                //
            } failure: { error in
                let errMsg = "\(error.1) (Code: \(error.0 ?? 0))"
                self.showErrorPopup(errMsg)            }

        }
    }
    
}

extension ListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else {
            return
        }
        if text.count > 3 && text.trimmingCharacters(in: .whitespaces).contains(" "){
            let vc = searchController.searchResultsController as? ResultTableViewController
            vc?.searchedText = text
        }
    }
    
    func willPresentSearchController(searchController: UISearchController)     {
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func willDismissSearchController(searchController: UISearchController) {
        self.navigationController?.navigationBar.isTranslucent = false
    }
}

extension ListViewController: ResultTableViewControllerDelegate {
    
    func selectedMovie(movieId: Int) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as? MovieDetailViewController {
            self.viewModel.openSelectedMovie(movieId: movieId, vc: vc, navigationController: navigationController) {
                //
            } failure: { error in
                let errMsg = "\(error.1) (Code: \(error.0 ?? 0))"
                self.showErrorPopup(errMsg)            }

        }
    }
}
