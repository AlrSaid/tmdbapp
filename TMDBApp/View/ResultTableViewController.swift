//
//  ResultTableViewController.swift
//  TMDBApp
//
//  Created by Said Alır on 6.06.2021.
//

import UIKit

protocol ResultTableViewControllerDelegate: AnyObject {
    func selectedMovie(movieId: Int)
}
class ResultTableViewController: UITableViewController {
    
    var searchedText = "" {
        didSet {
            updateData()
        }
    }
    var data: [SearchResult] = [] {
        didSet
        {
            self.tableView.reloadData()
        }
    }
    
    weak var delegate: ResultTableViewControllerDelegate? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    func updateData() {
        if self.searchedText.count < 3 {
            return
        }
        ListManager.shared.getSearchResult(searchText: searchedText) { data in
            self.data = data
        } failure: { error in
            print(error)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = data[indexPath.row].title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: false, completion: nil)
        self.delegate?.selectedMovie(movieId: self.data[indexPath.row].id)
    }

}
