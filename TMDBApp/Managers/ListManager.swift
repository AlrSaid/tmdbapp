//
//  ListManager.swift
//  TMDBApp
//
//  Created by Said Alır on 6.06.2021.
//

import Foundation
import Alamofire

class ListManager {
    static let shared = ListManager()
    
    func getUpcomingData(success: @escaping ([ListModel]) -> (), failure: @escaping (NetworkError) -> ()) {
        let str = "\(APIConstants.movieBaseUrl)" +
            "\(QueryParameters.upcoming.rawValue)" +
            "\(QueryParameters.apiKey.rawValue)" +
            "\(APIConstants.apiKey)"
        guard let url = URL(string: str) else {
            failure((0, "url init error"))
            return
        }
        AF.request(url).validate().responseJSON { response in
            if let error = response.error {
                failure((error.responseCode, error.localizedDescription))
            }
            if let data = response.data {
                do {
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(UpcomingModelResponse.self, from: data)
                    if let upcomingMovies = model.results {
                        success(upcomingMovies)
                    } else {
                        failure((0, "there is no upcoming movies"))
                    }
                } catch {
                    failure((0, "data parse error"))
                }
            }
        }
    }
    
    func getNowPlaying(success: @escaping ([NowPlayingModel]) -> (), failure: @escaping (NetworkError) -> ()) {
        let str = "\(APIConstants.movieBaseUrl)" +
            "\(QueryParameters.nowPlaying.rawValue)" +
            "\(QueryParameters.apiKey.rawValue)" +
            "\(APIConstants.apiKey)"
        guard let url = URL(string: str) else {
            failure((0, "url init error"))
            return
        }
        AF.request(url).validate().responseJSON { response in
            if let error = response.error {
                failure((error.responseCode, error.localizedDescription))
            }
            if let data = response.data {
                do {
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(NowPlayingModelResponse.self, from: data)
                    if let nowPlayingMovies = model.results {
                        success(nowPlayingMovies)
                    } else {
                        failure((0, "there is no now playing movies"))
                    }
                } catch {
                    failure((0, "data parse error"))
                }
            }
        }
    }
    
    func getSearchResult(searchText: String, success: @escaping ([SearchResult]) -> (), failure: @escaping (NetworkError) -> ()) {
        guard let encoded = searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        let str = "\(APIConstants.baseUrl)" +
            "\(QueryParameters.search.rawValue)" +
            "\(QueryParameters.apiKey.rawValue)" +
            "\(APIConstants.apiKey)" +
            "\(QueryParameters.query.rawValue)" +
            "\(encoded)"
        guard let url = URL(string: str) else {
            failure((0, "url init error"))
            return
        }
        AF.request(url).validate().responseJSON { response in
            if let error = response.error {
                failure((error.responseCode, error.localizedDescription))
            }
            if let data = response.data {
                do {
                    let decoder = JSONDecoder()
                    let arr = try decoder.decode(SearchResponse.self, from: data)
                    if let result = arr.results {
                        success(result)
                    } else {
                        failure((0, "no search result!!..."))
                    }
                } catch {
                    failure((0, "data parse error"))
                }
            }
        }
    }
}
