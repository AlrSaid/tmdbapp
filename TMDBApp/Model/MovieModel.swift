//
//  MovieModel.swift
//  TMDBApp
//
//  Created by Said Alır on 5.06.2021.
//

import Foundation

struct MovieModel: Codable {
    let id: Int
    let imdbId: String?
    let title: String
    let detail: String?
    let poster: String?
    let rating: Double
    let date: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case imdbId = "imdb_id"
        case title = "original_title"
        case detail = "overview"
        case poster = "backdrop_path"
        case rating = "vote_average"
        case date = "release_date"
    }
}

struct SimilarMovieResponse: Codable {
    var results: [SimilarMovie]?
}

struct SimilarMovie: Codable {
    var id: Int
    var title: String
    var image: String?
    var date: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case image = "poster_path"
        case date = "release_date"
    }
}

struct SearchResponse: Codable {
    var results: [SearchResult]?
}

struct SearchResult: Codable {
    var id: Int
    var title: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case title = "original_title"
    }
}
