//
//  Enums.swift
//  TMDBApp
//
//  Created by Said Alır on 5.06.2021.
//

import Foundation

typealias NetworkError = (Int?, String)

enum QueryParameters: String {
    case similar = "/similar"
    case apiKey = "?api_key="
    case upcoming
    case nowPlaying = "now_playing"
    case search = "search/movie"
    case query = "&query="
}
