//
//  ListModel.swift
//  TMDBApp
//
//  Created by Said Alır on 6.06.2021.
//

import Foundation

struct UpcomingModelResponse: Codable {
    var results: [ListModel]?
}

struct ListModel: Codable {
    var id: Int
    var title: String
    var img: String?
    var date: String
    var detail: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case title = "original_title"
        case img = "poster_path"
        case date = "release_date"
        case detail = "overview"
    }
}

struct NowPlayingModelResponse: Codable {
    var results: [NowPlayingModel]?
}

struct NowPlayingModel: Codable {
    var id: Int
    var title: String
    var img: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title = "original_title"
        case img = "backdrop_path"
    }
}

