//
//  ViewController.swift
//  TMDBApp
//
//  Created by Said Alır on 4.06.2021.
//

import UIKit
import Kingfisher

class MovieDetailViewController: BaseViewController {

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailTV: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var similarMoviesLabel: UILabel!
    @IBOutlet weak var similarMoviesCV: UICollectionView!
    var viewModel: MovieDetailViewModel!
    var openningNewMovie = false
    var data: [SimilarMovie] = [] {
        didSet {
            self.similarMoviesCV.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let img = self.viewModel.poster {
            self.movieImageView.kf.setImage(with: img, placeholder: UIImage(named: "cinema"))
        }
        self.navigationItem.title = self.viewModel.title
        self.titleLabel.text = self.viewModel.title
        self.detailTV.text = self.viewModel.detail
        self.ratingLabel.text = self.viewModel.rating
        self.dateLabel.text = self.viewModel.date
        self.viewModel.bindSimilarMovies = {
            self.data = self.viewModel.similarMovies
        }
        setupCollectionView()
    }
    
    func setupCollectionView() {
        self.similarMoviesCV.delegate = self
        self.similarMoviesCV.dataSource = self
        self.similarMoviesCV.register(UINib(nibName: "SimilarMovieCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "similar")
    }

    @IBAction func openIMDBpage(_ sender: Any) {
        if let path = self.viewModel.imdbId, let url =  URL(string: "\(APIConstants.imdbBase)\(path)"){
            UIApplication.shared.open(url)
        } else {
            let errMsg = NSLocalizedString("IMDB page not found!", comment: "")
            self.showErrorPopup(errMsg)
        }
    }
}


extension MovieDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "similar", for: indexPath) as? SimilarMovieCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.configureCell(model: data[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !self.openningNewMovie {
            self.openningNewMovie = true
            let id = self.data[indexPath.row].id
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as? MovieDetailViewController {
                self.viewModel.openSimilarMovie(id: id, vc: vc, navigationController: navigationController) {
                    self.openningNewMovie = false
                } failure: { error in
                    let errMsg = "\(error.1) (Code:\(error.0 ?? 0))"
                    self.showErrorPopup(errMsg)
                    self.openningNewMovie = false
                }

            }
        }
    }
    
}
