//
//  MovieDetailViewModel.swift
//  TMDBApp
//
//  Created by Said Alır on 4.06.2021.
//

import Foundation
import UIKit

class MovieDetailViewModel {
    var id: Int
    var imdbId: String?
    var title: String
    var detail: String
    var poster: URL?
    var rating: String
    var date: String
    var similarMovies: [SimilarMovie] = [] {
        didSet {
            self.bindSimilarMovies()
        }
    }
    var bindSimilarMovies: (() -> ()) = {}

    
    init(model: MovieModel) {
        self.id =  model.id
        self.title = model.title
        self.detail = model.detail ?? "No detail"
        if let path = model.poster {
            self.poster = URL(string: "\(APIConstants.posterBaseUrl)\(path)")
        } else {
            self.poster = nil
        }
        self.date = model.date ?? "UNKNOWN"
        self.rating = "\(model.rating)"
        self.imdbId = model.imdbId
        MovieManager.shared.getSimilarMovies(movieId: self.id) { movies in
            self.similarMovies = movies
        } failure: { error in
            print(error)
        }
    }
    
    func openSimilarMovie(id: Int,
                          vc: MovieDetailViewController,
                          navigationController: UINavigationController?,
                          success: @escaping () -> (),
                          failure: @escaping (NetworkError) -> ()) {
        MovieManager.shared.getMovieDetail(movieId: id) { model in
            let vm = MovieDetailViewModel(model: model)
            vc.viewModel = vm
            navigationController?.pushViewController(vc, animated: true)
            success()
        } failure: { error in
            failure(error)
        }

    }
}
